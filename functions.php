<?php 

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_uri(),
        array( 'parenthandle' ), 
        wp_get_theme()->get('Version') // this only works if you have Version in the style header
    );
}

function theme_gsap_script() {
    wp_enqueue_script( 'gsap-js', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/gsap.min.js', array(), false, true );
    wp_enqueue_script( 'Scrolltrigger', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/ScrollTrigger.min.js', array(), false, true );
}

add_action( 'wp_enqueue_scripts', 'theme_gsap_script' );

add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        // register a countup block.
        acf_register_block_type(array(
            'name'              => 'CounterUp',
            'title'             => __('nombre incrementé'),
            'description'       => __('A custom countup with trigger on scroll.'),
            'render_template'   => 'template-parts/blocks/counterup/counterup.php',
            'category'          => 'formatting',
            'icon'              => 'admin-comments',
            'align'             => 'full',
            'keywords'          => array( 'counter', 'quote' ),
            'enqueue_style'     => get_template_directory_uri() . '-child/template-parts/blocks/counterup/counterup.css',
            'enqueue_script'    => get_template_directory_uri() . '-child/template-parts/blocks/counterup/counterup.js',
        ));

        // register a mosaique block.
        acf_register_block_type(array(
            'name'              => 'mosaique',
            'title'             => __('mosaïque'),
            'description'       => __('A mosaique block of text & image.'),
            'render_template'   => 'template-parts/blocks/mosaique/mosaique.php',
            'category'          => 'formatting',
            'icon'              => 'admin-comments',
            'keywords'          => array( 'mosaique', 'quote' ),
            'enqueue_style'     => get_template_directory_uri() . '-child/template-parts/blocks/mosaique/mosaique.css',
        ));
    }
}
