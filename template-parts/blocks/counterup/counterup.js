"use strict";
function docReady(fn) {

    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

docReady (() => {
    const countupEls = document.querySelectorAll( '.counter-number' );
    console.log(countupEls)
    
    //Set the animation duration 
    const animationDuration = 1000;
    //Set the fps(60fps)
    const frameDuration = 1000 / 60;

    const totalFrames = Math.round( animationDuration / frameDuration );

    //Set easing animation
    const easeOutQuad = t => t * ( 2 - t );

    const animateCountUp = el => {
        let frame = 0;
        el.innerHTML = 0
        const countTo = parseInt( el.dataset.to, 10 );
        
        const counter = setInterval( () => {
            frame++;

            const progress = easeOutQuad( frame / totalFrames );

            const currentCount = Math.round( countTo * progress );


            if ( parseInt( el.innerHTML, 10 ) !== currentCount ) {
                el.innerHTML = currentCount;
            }

            if ( frame === totalFrames ) {
                clearInterval( counter );
            }
        }, frameDuration );
    };
    ScrollTrigger.create({
        trigger: ".counter-container",
        start: "top bottom-=300px",
        markers: true,
        onEnter:() => countupEls.forEach( animateCountUp ),
        once: true
    });
})
