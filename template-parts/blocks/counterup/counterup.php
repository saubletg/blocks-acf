<?php
if( have_rows('counter_up') ):?>
    <div class="counter-container">
        <?php
        while( have_rows('counter_up') ) : the_row();
            $label = get_sub_field('label'); 
            $number = get_sub_field('number');
            ?>
            <span class="counter-label"><?= $label; ?></span> 
            <span class="counter-number" data-to=<?= $number; ?>>0</span>
        <?php endwhile;?>
    </div>
    <?php
else :
endif;?>