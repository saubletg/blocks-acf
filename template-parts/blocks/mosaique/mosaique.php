<!-- mosaic block 2/3/2 -->
<div class="mosaic-wrapper">
    <div class="mosaic-first-paragraph-wrapper element">
        <?php 
            $background_image = get_field('background_image');
            $background_image_size = 'full'; 
            if( $background_image ) {
                echo wp_get_attachment_image( $background_image, $background_image_size,"", ["class" => "mosaic-background-img"]);
        }?>
        <h3 class="mosaic-title"><?php the_field('title_first');?></h3>
        <p class="mosaic-text"><?php the_field('first_paragraph');?> </p>
    </div>
    <div class="mosaic-first-img-wrapper element">
        <?php 
            $first_img_image = get_field('first_img');
            $first_img_size = 'full';
            if( $background_image ) {
                echo wp_get_attachment_image( $first_img_image, $first_img_size,"", ["class" => "mosaic-img"]);
        }?>
    </div>
    <div class="mosaic-second-img-wrapper element">
        <?php 
            $second_img_image = get_field('second_img');
            $second_img_size = 'full';
            if( $background_image ) {
                echo wp_get_attachment_image( $second_img_image, $second_img_size,"", ["class" => "mosaic-img"]);
        }?>
    </div>
    <div class="mosaic-logo-wrapper element">
        <?php 
            $logo_image = get_field('logo');
            $logo_size = 'full';
            if( $background_image ) {
                echo wp_get_attachment_image( $logo_image, $logo_size,"", ["class" => "logo"]);
        }?>
    </div>
    <div class="mosaic-third-img-wrapper element">
        <?php 
            $third_img_image = get_field('third_img');
            $third_img_size = 'full';
            if( $background_image ) {
                echo wp_get_attachment_image( $third_img_image, $third_img_size,"", ["class" => "mosaic-img"]);
        }?>
    </div>
    <div class="mosaic-fourth-img-wrapper element">
        <?php 
            $fourth_img_image = get_field('fourth_img');
            $fourth_img_size = 'full';
            if( $background_image ) {
                echo wp_get_attachment_image( $fourth_img_image, $fourth_img_size,"", ["class" => "mosaic-img"]);
        }?>
    </div>
    <div class="mosaic-second-paragraph-wrapper element">
        <h3 class="mosaic-title"><?php the_field('title_second');?></h3>
        <p class="mosaic-text"><?php the_field('second_paragraph');?></p>
        <div class="has-bleu-background-color">
            <?php 
                $link = get_field('link');
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
            <?php endif; ?>
        </div>
        
    </div>
</div>